<?php
return array(
	'Eloquent' => Illuminate\Database\Eloquent\Model::class,
	'Request' => Symfony\Component\HttpFoundation\Request::class,
	'Response' => Symfony\Component\HttpFoundation\Response::class,
	'Blade' => Philo\Blade\Blade::class,
)

?>