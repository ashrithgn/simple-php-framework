<?php

function HashPassword($password) {
	$passwordConfig = include __Dir__ . '/../Config/Auth.php';
	$temp = $passwordConfig['prefix_salt'] . $password . $passwordConfig['suffix_salt'];
	return password_hash($temp, constant($passwordConfig['method']));
}

function AuthCheck($data = array()) {

	$passwordConfig = include __Dir__ . '/../Config/Auth.php';
	if (isset($data['password']) && count($data) > 1) {
		$user = call_user_func('App\\Model\\' . $passwordConfig['model'] . '::select');
		foreach ($data as $key => $value) {
			if ($key != 'password') {
				$user->where($key, '=', $value);
			}
		}
		if(!empty($result)){
			$result = @$user->first();
			if (password_verify($passwordConfig['prefix_salt'] . $data['password'] . $passwordConfig['suffix_salt'], $result->password)) {
				return $result;
			}
		}
		return false;
	}
	return false;
	exit;
}

function view($name = null, $data = null, $responseCode = 200, $responseMessage = "ok") {
	$response = new \Response();
	$blade = new \Blade(__Dir__ . '/../Views', __Dir__ . "/../../storage/cache");
	$response->setStatusCode($responseCode, $responseMessage);
	$response->setContent($blade->view()->make($name)->with((array) $data)->render());
	$response->send();
}

function json($data = null, $responseCode = "200", $responseMessage = "ok") {
	$response = new Response();
	$response->headers->set('Content-Type', 'application/json');
	$response->setContent(json_encode($data));
	$response->setStatusCode($responseCode, $responseMessage);
	$response->send();
}